package com.outwardInc.commonutils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

public class BaseClass {
	
	public WebDriver driver;
	public WebDriverWait wait;
	

	public BaseClass(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Constants.TIMEOUT, Constants.POLLING);
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, Constants.TIMEOUT), this);
    }
	void waitForElementToAppear(By locator) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
	
	public void visibilityOfElement(By elementBy) {
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementBy));
	}
	
	/* Click on element */
	public void clickElement(By elementBy) {
		visibilityOfElement(elementBy);
		driver.findElement(elementBy).click();
		Reporter.log("Element is clicked");
		}
	/* Read element */
	public String readText(By elementBy) {
		visibilityOfElement(elementBy);
		return driver.findElement(elementBy).getText();
		}
	
	
	public String readpropertyfile(String configElement){
		String Element =PropertyFileUtility.getProperty(configElement);
		return Element;
	}
	
	public void clickHiddenElement(WebElement ele) {
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", ele);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	
	 public String[] getSplitStringByComma(String s1) {
         String[] dataSeqArray = s1.split(",");
         System.out.println("Total Count is "+dataSeqArray.length);
         for(int i=0;i<dataSeqArray.length;i++){
        	 System.out.println(dataSeqArray[i]);
        	 }
         return(dataSeqArray);
         }
}
