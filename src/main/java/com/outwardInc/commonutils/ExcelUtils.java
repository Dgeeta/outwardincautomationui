package com.outwardInc.commonutils;

import java.io.File;

import java.io.FileInputStream;

import java.io.FileOutputStream;

import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.ss.usermodel.Cell;

import org.apache.poi.ss.usermodel.Row;

import org.apache.poi.ss.usermodel.Sheet;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {
	
               public static String[][] readFromExcel(String filePath, String sheetName) throws IOException {
	           File file = new File(filePath);
	           FileInputStream inputStream = new FileInputStream(file);
	           XSSFWorkbook myWB = new XSSFWorkbook (inputStream);
	           Sheet sheet = myWB.getSheet(sheetName);
	           System.out.println("las row num: " + sheet.getLastRowNum());
	           System.out.println("first row: " + sheet.getFirstRowNum());
	           int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
	           int colCount = sheet.getRow(0).getLastCellNum();
	           String[][] excel = new String[rowCount][colCount];

	           for (int i = 0; i < rowCount; i++) {
	        	   org.apache.poi.ss.usermodel.Row row = sheet.getRow (i+1);

	        	   for (int j = 0; j < row.getLastCellNum(); j++) {
	        		   excel[i][j] = row.getCell(j).toString();
	        	   }
	           }
	          // myWB.close();
	           return excel;
    }
//	
//	
///*	
//	 public void readExcel(String filePath,String fileName,String sheetName) throws IOException{
//		 File file =    new File(filePath+"\\"+fileName);
//		 FileInputStream inputStream = new FileInputStream(file);
//		 Workbook Wb = null;
//		 Wb = new XSSFWorkbook(inputStream);
//		 Wb = new HSSFWorkbook(inputStream);
//		 Sheet sheet = Wb.getSheet(sheetName);
//
//		    //Find number of rows in excel file
//
//		    int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
//
//		    //Create a loop over all the rows of excel file to read it
//
//		    for (int i = 0; i < rowCount+1; i++) {
//
//		        Row row = sheet.getRow(i);
//
//		        //Create a loop to print cell values in a row
//
//		        for (int j = 0; j < row.getLastCellNum(); j++) {
//
//		            //Print Excel data in console
//
//		            System.out.print(row.getCell(j).getStringCellValue()+"|| ");
//
//		        }
//
//		        System.out.println();
//		    } 
//
//		    }  
//	
//	
//	
//	
//	
//	
//    public void writeExcel(String filePath,String fileName,String sheetName,String[] dataToWrite) throws IOException{
//
//        File file =    new File(filePath+"\\"+fileName);
//        FileInputStream inputStream = new FileInputStream(file);
//        Workbook wb = null;
//        wb = new XSSFWorkbook(inputStream);
//        Sheet sheet = wb.getSheet(sheetName);
//        int rowCount = sheet.getLastRowNum()-sheet.getFirstRowNum();
//        Row row = sheet.getRow(0);
//        Row newRow = sheet.createRow(rowCount+1);
//        
//        for(int j = 0; j < row.getLastCellNum(); j++){
//        	Cell cell = newRow.createCell(j);
//        	cell.setCellValue(dataToWrite[j]);
//        	}
//
//	
//
//    }
//    
//    */
}
