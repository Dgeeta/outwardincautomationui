package com.outwardInc.commonutils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

   public class PropertyFileUtility {
	   private static Properties obj; 
	   private static final String fileName=".\\src\\main\\resources\\ConfigurationFile\\Config.properties";

/** --------------------Reading property file-------------------- */
        public static String getProperty(String key) {
        	String value = null;
        	loadProperties();
        	value = obj.getProperty(key);
        	return value;
        	}
        
        
        public static void loadProperties() {
        	try {
        		obj = new Properties();
        		FileReader fileReader = new FileReader(fileName);
        		obj.load(fileReader);
        		if (fileReader != null) {
        			fileReader.close();
        			}
        		}
        	catch (FileNotFoundException e) {
        		// TODO Auto-generated catch block
        		e.printStackTrace();
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        			}
        	}
        }


