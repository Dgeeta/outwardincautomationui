package com.outwardInc.uipages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.outwardInc.commonutils.BaseClass;

public class ZoomClass extends BaseClass {
	
	By cancelButton=By.xpath("//*[@class='owFullscreenCloseButton pointer']");
	By viewTop=By.xpath("//*[@id='owTopDownToggle']");
	By DimensionToggle=By.xpath("//*[@id='owDimensionsToggle']");
	By largerImageCanvasId=By.xpath("//*[@id='owViewport']");
	By hiddenRotationButton=By.xpath("//button[@id ='rotationstatus']");
	By hiddenDimensionToggle=By.xpath("//*[@id='dimensionsstatus']");
	By hiddenTopToggle=By.xpath("//*[@id='topviewstatus']");
	By LargerImageToggle=By.xpath("//*[@id ='owFullscreenToggle']");
	By hiddenZoomToggle=By.xpath("//*[@id='zoomstatus']");
	By zoomFeature=By.xpath("//*[@id='owInlineZoomToggle']");
	
	
	
	public ZoomClass(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void validationOfZoomToggle() throws InterruptedException {
		SoftAssert softAssert = new SoftAssert();
		WebElement ele = driver.findElement(zoomFeature);
		WebElement hiddenEle= driver.findElement(hiddenZoomToggle);
		String originalImageResolution=getImageResolution(hiddenEle);
		Reporter.log("Fetching the Resolution of Original Image, The current resolution is: "+originalImageResolution);
		Reporter.log("Going to check if the button is Highlighted or Not");
		boolean originalToggleStatus=getZoomToggleHighlightStatus(ele);
		Reporter.log("Now, Going to click on Zoom In Toggle");
		clickElement(zoomFeature);
		Thread.sleep(3000);
		Reporter.log(" After click on Zoom in Toggle, going to check if the button is Highlighted or Not");
		boolean newToggleStatus=getZoomToggleHighlightStatus(ele);
		softAssert.assertNotEquals(newToggleStatus, originalToggleStatus, "New Toggle Status and original Toggle Status, Both are same");
		String zoomImageResolution=getImageResolution(hiddenEle);
		Reporter.log("Now the current resolution changed to: "+zoomImageResolution);
		boolean ZoomResolutionChangeStatus= compareImageResolution(zoomImageResolution, originalImageResolution);
		softAssert.assertEquals(ZoomResolutionChangeStatus, true);
		softAssert.assertAll();
		
		}
	
	public void validationOfZoomOutToggle() throws InterruptedException {
		SoftAssert softAssert = new SoftAssert();
		WebElement ele = driver.findElement(zoomFeature);
		WebElement hiddenEle= driver.findElement(hiddenZoomToggle);
		String originalImageResolution=getImageResolution(hiddenEle);
		Reporter.log("Fetching the Resolution of Image, The current resolution is: "+originalImageResolution);
		Reporter.log("Going to check if the button is Highlighted or Not");
		boolean originalToggleStatus=getZoomToggleHighlightStatus(ele);
		//softAssert.assertEquals(ZoomToggleStatus, true);
		Reporter.log("Now, Going to click on Zoom Out Toggle");
		clickElement(zoomFeature);
		Thread.sleep(2000);
		Reporter.log(" After click on Zoom out Toggle, going to check if the button is Highlighted or Not");
		boolean newToggleStatus=getZoomToggleHighlightStatus(ele);
		softAssert.assertNotEquals(newToggleStatus, originalToggleStatus, "New Toggle Status and original Toggle Status, Both are same");
	//	softAssert.assertEquals(ZoomToggleStatus, false);
		String zoomImageResolution=getImageResolution(hiddenEle);
		Reporter.log("Now the current resolution changed to: "+zoomImageResolution);
		boolean ZoomResolutionChangeStatus= compareImageResolution(zoomImageResolution, originalImageResolution); 
	//	softAssert.assertEquals(ZoomResolutionChangeStatus, false);
	    softAssert.assertAll();
		}
	
		
	public boolean getZoomToggleHighlightStatus(WebElement ele) {
		String buttonHighlightStatus=ele.getAttribute("class");
		boolean ZoomToggleStatus=false;
		if(buttonHighlightStatus.equals("active")) {
			Reporter.log("The Zoom button is Highlighted");
			 ZoomToggleStatus=true;
			}
		else {
			Reporter.log("The Zoom button is not yet Highlighted");
		 ZoomToggleStatus=false;
		 }
		return ZoomToggleStatus;
		}
	
	
	public boolean compareImageResolution(String zoomedImage, String originalImage) {
		boolean ZoomResolutionChangeStatus=false;
		if (zoomedImage.equals(originalImage)) {
			Reporter.log("Both the Resolutions are similar");
			ZoomResolutionChangeStatus=false;
		}
		else {
			Reporter.log("The Image Resolution got changed");
		ZoomResolutionChangeStatus=true;}
		return ZoomResolutionChangeStatus;
		}
			
	public String getImageResolution(WebElement hiddenEle) {
		Reporter.log("Going to get the Resolution of the Current Image ");
		String dataResolution=hiddenEle.getAttribute("data-resolution");
		Reporter.log("Current Image Resolution is : "+dataResolution);
		return (dataResolution);
		}
	
	public void validateZoomImagewithDimensionToggle() throws InterruptedException {
		SoftAssert softAssert = new SoftAssert();
		boolean ToggleStatus=false;
		clickDimensionToggle();
		Reporter.log("Going to click on Zoom In Toggle");
		clickElement(zoomFeature);
		WebElement ele= driver.findElement(hiddenDimensionToggle);
		WebElement hiddenEle= driver.findElement(hiddenZoomToggle);
		getImageResolution(hiddenEle);
		String dimensionValue=ele.getAttribute("value");
		String zoomValue=hiddenEle.getAttribute("value");
		Reporter.log("The Dimension Toggle's Activeness status is "+dimensionValue);
		Reporter.log("The Zoom Toggle's Activeness status is "+zoomValue);
		softAssert.assertEquals(dimensionValue, "on","The Dimension is missing as the the Toggle's status is Inactive");
		if (dimensionValue.equals(zoomValue)){
			Reporter.log("The Image is opened in Zoomed In status with Dimensions On");
			ToggleStatus=true;
			clickDimensionToggle();
			}
		else {
			ToggleStatus=false;
			}
		softAssert.assertEquals(ToggleStatus, true, "The Dimension is missing, as the status of Toggle is Inactive");
		clickElement(zoomFeature);
		getImageResolution(hiddenEle);
		softAssert.assertAll();
	}

	public void verifyZoomImageWithTopView() {
		SoftAssert softAssert = new SoftAssert();
		boolean ToggleStatus=false;
		clickElement(viewTop);
		WebElement ele= driver.findElement(hiddenTopToggle);
		Reporter.log("Going to click on Zoom In Toggle");
		clickElement(zoomFeature);
		WebElement hiddenEle= driver.findElement(hiddenZoomToggle);
		getImageResolution(hiddenEle);
		String topViewValue=ele.getAttribute("value");
		String zoomValue=hiddenEle.getAttribute("value");
		Reporter.log("The TopView Toggle's Activeness status is "+topViewValue);
		Reporter.log("The Zoom Toggle's Activeness status is "+zoomValue);
		if (topViewValue.equals(zoomValue)){
			ToggleStatus=true;
			Reporter.log("The Image is opened in Zoomed In status with Top View Mode On");
			Reporter.log("The Toggle's current Status is "+ToggleStatus);
			}
		else {
			Reporter.log("Something wrong with the Top view/Zoom");
			ToggleStatus=false;
			}
		softAssert.assertEquals(ToggleStatus, true, "The Top View/Zoom is missing, as the status of Toggle is Inactive");
		clickElement(zoomFeature);
		getImageResolution(hiddenEle);
		clickElement(viewTop);
		softAssert.assertAll();
	}
	
	
	public void validateZoomFeatureWithDimensionAndTopView() throws InterruptedException {
		SoftAssert softAssert = new SoftAssert();
		boolean ToggleStatus=false;
		clickDimensionToggle();
		WebElement dimensionEle= driver.findElement(hiddenDimensionToggle);
		Reporter.log("Going to click on View Top Toggle");
		clickElement(viewTop);
		WebElement ele= driver.findElement(hiddenTopToggle);
		Reporter.log("Going to click on Zoom In Toggle");
		clickElement(zoomFeature);
		WebElement hiddenEle= driver.findElement(hiddenZoomToggle);
		getImageResolution(hiddenEle);
		String dimensionValue=dimensionEle.getAttribute("value");
		String topViewValue=ele.getAttribute("value");
		String zoomValue=hiddenEle.getAttribute("value");
		Reporter.log("The TopView Toggle's Activeness status is "+topViewValue);
		Reporter.log("The Zoom Toggle's Activeness status is "+zoomValue);
		softAssert.assertEquals(dimensionValue, "on", "The Dimension is Missing, As the Toggle is not active");
		softAssert.assertEquals(topViewValue, "on", "The Top View is Missing, As the Toggle is not active");
		softAssert.assertEquals(zoomValue, "on", "The Zoom In Functionality is Missing, As the Toggle is not active");
		
		if (topViewValue.equals(zoomValue)){
			Reporter.log("The Image is opened in Zoomed In status with Top View Mode On");}
		if(zoomValue.equals(dimensionValue)) {
			ToggleStatus=true;
			}
		else {
			Reporter.log("Something wrong with the Dimensions");
			ToggleStatus=false;
			}
		softAssert.assertEquals(ToggleStatus, true, " Test Case Failed- The Top View/Zoom/Dimension is missing, as the status of Toggle is Inactive");
		clickElement(zoomFeature);
		getImageResolution(hiddenEle);
		clickElement(viewTop);
		softAssert.assertAll();
	}
	
	
	public void ToggleHighlightStatus(By element) {
		String buttonHighlightStatus=driver.findElement(element).getAttribute("value");
		if(buttonHighlightStatus.equals("on")) {
			Reporter.log("The Toggle is Highlighted");
			}
		else
			Reporter.log("The Toggle is not yet Highlighted");
		}
	
	public void clickDimensionToggle() throws InterruptedException {
		WebElement ele= driver.findElement(DimensionToggle);
		Reporter.log("Going to click on Dimension Toggle");
		clickElement(DimensionToggle);
		Thread.sleep(1900);
		}
	
	
	
	
	
}
