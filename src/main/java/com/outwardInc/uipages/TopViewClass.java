package com.outwardInc.uipages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.outwardInc.commonutils.BaseClass;

public class TopViewClass extends BaseClass {
	
	By cancelButton=By.xpath("//*[@class='owFullscreenCloseButton pointer']");
	By viewTop=By.xpath("//*[@id='owTopDownToggle']");
	By DimensionToggle=By.xpath("//*[@id='owDimensionsToggle']");
	By largerImageCanvasId=By.xpath("//*[@id='owViewport']");
	By hiddenRotationButton=By.xpath("//button[@id ='rotationstatus']");
	By hiddenDimensionToggle=By.xpath("//*[@id='dimensionsstatus']");
	By hiddenTopToggle=By.xpath("//*[@id='topviewstatus']");
	By LargerImageToggle=By.xpath("//*[@id ='owFullscreenToggle']");
	By hiddenZoomToggle=By.xpath("//*[@id='zoomstatus']");
	By zoomFeature=By.xpath("//*[@id='owInlineZoomToggle']");
	SoftAssert softAssert = new SoftAssert();
	
	public TopViewClass(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void clickOnViewFront() throws InterruptedException {
		//	clickElement (HiddenTopToggle) ;
			Thread.sleep(2000);
			}
     
	public void getCurrentViewMode(WebElement ele) {
		String currentViewTopMode= ele.getAttribute("value");
		Reporter.log("Getting the current Element Attribute, The Value is : "+currentViewTopMode);
		if (currentViewTopMode.equals("off")){
				Reporter.log("currently, The Image is in -View front Mode ");	
		}
		
		else
			Reporter.log("currently, Can't determine the Mode of the Image ");
		}
	
	public void getNewViewMode(WebElement ele) {
		String newViewTopMode= ele.getAttribute("value");
		Reporter.log("Getting the Element Attribute "+newViewTopMode);
		if (newViewTopMode.equals("on")){
				Reporter.log("currently, The Image is in -View Top Mode ");	
		}
		
		else
			Reporter.log("currently, Can't determine the Mode of the Image ");
		}
	
	
	public void viewTopImage() throws InterruptedException{
		WebElement ele= driver.findElement(hiddenTopToggle);
		getCurrentViewMode(ele);
		Reporter.log("Going to click on - View Top Image");
		clickHiddenElement(ele);
		getNewViewMode(ele);
		Reporter.log("Now Going to click on - View large Image button, "
				+ "when Image is already opened in Top View");
	//	viewLargerImageFunc();
		Reporter.log("Going to click on - View Front Image");
		//WebElement eles=driver.findElement(viewTop);
		clickElement(viewTop);
		Reporter.log("The Image is in Normal, View -Front view Mode ");
		getCurrentViewMode(ele);
		
		}
	
	
	
	public void clickOnviewTopImage() throws InterruptedException {
		String currentToggleStatus=null;
		SoftAssert softAssert = new SoftAssert();
		WebElement ele= driver.findElement(hiddenTopToggle);
		getTopViewAttributeValue();
		clickOnTopToggle();
		getTopViewAttributedataFrame();
		getTopViewAttributeValue();
		getCurrentViewMode(ele);
		String originalToggleStatus=ele.getAttribute("value");
		softAssert.assertEquals(originalToggleStatus, "on", "The view Top toggle should be highlighted");
		Reporter.log("The Data Top View Frame used for the Rendered Image is :"+ele.getAttribute("data-topviewframe"));
		clickOnViewFrontToggle2();
		currentToggleStatus=ele.getAttribute("value");;
		Reporter.log("Going to validate if the Image is in Top view or Front view");
		String classStatus=ele.getAttribute("value");
		if(classStatus.contains("off")) {
			Reporter.log("The Image is in Normal State ie. Front view Mode ");
		}
		else {
			Reporter.log("The Rendered Image is in Top view Mode");
			
		}
		softAssert.assertEquals(currentToggleStatus, "off", "The view Front toggle should not be highlighted");
		softAssert.assertAll();
		
		}
	

	public void clickOnViewFrontToggle() {
		Reporter.log("Going to click on view Front Toggle");
		clickElement(viewTop);
		WebElement hiddenEle= driver.findElement(hiddenTopToggle);
		Reporter.log("Going to validate if the Image is in Top view or Front view");
		String classStatus=hiddenEle.getAttribute("value");
		if(classStatus.contains("off")) {
			Reporter.log("The Image is in Normal State ie. Front view Mode ");
		}
		else {
			Reporter.log("The Rendered Image is in Top view Mode");
			
		}
	}
	
	public void getTopViewAttributeValue() {
		WebElement hiddenEle= driver.findElement(hiddenTopToggle);
		String getvalue=hiddenEle.getAttribute("value");
		Reporter.log("Fetching the Current status of the Rendered Image, Which is :"+getvalue);
		if (getvalue.equals("off")){
			Reporter.log("currently, The Image is in -View front Mode ");
			}
		else
		    Reporter.log("currently, The Image is in -View Top Mode ");
		}
	
	public void getTopViewAttributedataFrame() {
		WebElement hiddenEle= driver.findElement(hiddenTopToggle);
		String getDataFrame=hiddenEle.getAttribute("data-topviewframe");
		Reporter.log("Fetching the Current Data frame of the Rendered Image :"+getDataFrame);
		}
	
	public void clickOnTopToggle() throws InterruptedException {
		Reporter.log("Going to click on View Top Toggle");
		clickElement(viewTop);
		Thread.sleep(3000);
		}
	
	public void clickOnViewFrontToggle2() throws InterruptedException {
		Reporter.log("Going to click on View Front Toggle");
		clickElement(viewTop);
		Thread.sleep(2000);
		}
	
}
