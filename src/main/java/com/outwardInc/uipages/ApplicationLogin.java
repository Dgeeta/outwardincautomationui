package com.outwardInc.uipages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;
import org.testng.log4testng.Logger;

import com.outwardInc.commonutils.BaseClass;

public class ApplicationLogin extends BaseClass{
	
	By OutwardUserName=By.xpath("//input[@name ='username']");
	By OutwardPassword=By.xpath("//input[@name ='password']");
	By OutwardLoginButton=By.xpath("//*[@name ='login']");
	By outwardLoginBtn=By.xpath("//a[text()='Outward Login']");
	SoftAssert softAssert = new SoftAssert();
	
	 public ApplicationLogin(WebDriver driver) {
	        super(driver);
	    }
	

	public void loginIntoApplication(String username, String password) throws InterruptedException{
		driver.findElement(OutwardUserName).sendKeys(username);
		driver.findElement(OutwardPassword).sendKeys(password);
		clickElement(OutwardLoginButton);
		String PageTitle ="Admin Panel";
		String pageTitle= driver.getTitle();
		softAssert.assertEquals(pageTitle, PageTitle);
		softAssert.assertAll();

		}
	
	public void clickComposerLink(String composerLink){
		driver.get(composerLink);
		}

}
