package com.outwardInc.uipages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.outwardInc.commonutils.BaseClass;

public class RotatorClass extends BaseClass {
	
	By hiddenRotationButton=By.xpath("//button[@id ='rotationstatus']");

	
	public RotatorClass(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public String getRotationTypeOfImage() {
		SoftAssert softAssert = new SoftAssert();
		Reporter.log("calling to get the Rotation Type of Rendered Image");
		WebElement ele= driver.findElement(hiddenRotationButton);
		String rotationStatus="fail";
		String rotationType=ele.getAttribute("data-rotationtype");
		if (rotationType!=null &&((rotationType.contentEquals("180"))||rotationType.contentEquals("360"))) {
			rotationStatus="pass";
			}
		softAssert.assertEquals(rotationStatus, "pass", "The Rotation Type is either Null or can't be identified");
		softAssert.assertAll();
		return(rotationType);
		}
	

	public String getFrameSequenceOfImage() {
		Reporter.log("Fetching the Frame Sequence of the Image");
		WebElement ele= driver.findElement(hiddenRotationButton);
		String dataFrameSeq=ele.getAttribute("data-framesequence");
		return(dataFrameSeq);
		}
	
	public String getTopDownFrameSequenceOfImage() {
		SoftAssert softAssert = new SoftAssert();
		boolean isFramevailable=false;
		String topDownDataFrameSeq=null;
		Reporter.log("Fetching the TopDown Frame Sequence of the Image");
		WebElement ele= driver.findElement(hiddenRotationButton);
		topDownDataFrameSeq=ele.getAttribute("data-topdown-framesequence");
		Reporter.log("Getting the Value of TopDown Frame, Which is: "+topDownDataFrameSeq);
		if(topDownDataFrameSeq.equals(null)) {
			isFramevailable=false;
			}
		else
			isFramevailable=true;
		softAssert.assertEquals(isFramevailable,true, "All the TopDown Frames are missing ie TopDownS0002, TopDownS0004,"
				+ "TopDownS0006, TopDownS0008, TopDownS0010, TopDownS0012");
		softAssert.assertAll();
		return(topDownDataFrameSeq);
		}
	
	public void validateImageRotation() {
		SoftAssert softAssert = new SoftAssert();
		int numberOfRotation=20;
		WebElement ele= driver.findElement(hiddenRotationButton);
		String value=ele.getAttribute("value");
		//softAssert.assertEquals("2", value);
		Reporter.log("Going to Rotate the Image ");
		if (getRotationTypeOfImage().equals("360"))
		{
			numberOfRotation = 32;
		}
		else if (getRotationTypeOfImage().equals("180")) {
			numberOfRotation = 16;
		}
		for (int i=1; i <=numberOfRotation; i++) {	
			clickHiddenElement(ele);
			Reporter.log("The Image is rotated and its current Data-Frame is 360S000"+ele.getAttribute("value"));
			}
		
		softAssert.assertAll();
		}
	
	public void verifyMissingDataFrame() {
		SoftAssert softAssert = new SoftAssert();
		boolean missingFrame=true;
		String dataSequence=getFrameSequenceOfImage();
		String[] Framecount= getSplitStringByComma(dataSequence);
		Reporter.log("Going to check if any Frame for the Rotator is Missing");
		if(Framecount.length==31 ||Framecount.length==32 || Framecount.length==15 ||Framecount.length ==16) {
			missingFrame=false;
			Reporter.log("No frame is missing , Total data sequence frame count available is "+Framecount.length);
			}
		
		else {
			displayMissingDataFrames(Framecount);
		}
		softAssert.assertEquals(missingFrame, false, "Total Frame count should be either 31, 32, 15 or 16; But Total Frame"
				+ "calculated is: "+Framecount.length);
		softAssert.assertAll();
	}
	
	public void verifyMissingTopDownFrame() {
		SoftAssert softAssert = new SoftAssert();
		boolean missingFrame=true;
		String topDownDataFrameSeq=getTopDownFrameSequenceOfImage();
		String[] Framecount= getSplitStringByComma(topDownDataFrameSeq);
		Reporter.log("Going to check if any Top Down Frame for the Rotator is Missing or Not");
		if(Framecount.length==7) {
			missingFrame=false;
			Reporter.log("Total FrameCount available is "+Framecount.length);
			}
		else {
			Reporter.log("Total TopDown frame count should be 7, Few Frames are missing");
			displayMissingTopDownFrames(Framecount);
			
		}
		softAssert.assertEquals(missingFrame, false, "Total Frame count should be 7; But Total Frame"
				+ "calculated is: "+Framecount.length);
		softAssert.assertAll();
	}
	
	
	public void displayMissingTopDownFrames(String[] Framecount) {
		Reporter.log("Top Down Frames are missing, Check logs for finding the missing frames");
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(Framecount[1], "TopDownS0002", "Top Down Frame Sequence : TopDownS0002 is missing");
		softAssert.assertEquals(Framecount[1], "TopDownS0004", "Top Down Frame Sequence : TopDownS0004 is missing");
		softAssert.assertEquals(Framecount[1], "TopDownS0006", "Top Down Frame Sequence : TopDownS0006 is missing");
		softAssert.assertEquals(Framecount[1], "TopDownS0008", "Top Down Frame Sequence : TopDownS0008 is missing");
		softAssert.assertEquals(Framecount[1], "TopDownS0010", "Top Down Frame Sequence : TopDownS0010 is missing");
		softAssert.assertEquals(Framecount[1], "TopDownS0012", "Top Down Frame Sequence : TopDownS0012 is missing");
		softAssert.assertEquals(Framecount[1], "TopDownS0014", "Top Down Frame Sequence : TopDownS0014 is missing");
		softAssert.assertAll();
		}
	
	public void displayMissingDataFrames(String[] Framecount){
		Reporter.log("Data Frames are missing, check logs for finding the missing frames");
		SoftAssert softAssert = new SoftAssert();
		String currentDataFrame;
		WebElement ele= driver.findElement(hiddenRotationButton);
		String initialRotationValue=ele.getAttribute("value");
		currentDataFrame=initialRotationValue;
		for(int i=0; i<32;i++) {
			ele.click();
			initialRotationValue=ele.getAttribute("value");
			softAssert.assertEquals(initialRotationValue, currentDataFrame+i, "The Missing frame is 3600S00:"+currentDataFrame+i);
		}
		
		
	}
	

}
