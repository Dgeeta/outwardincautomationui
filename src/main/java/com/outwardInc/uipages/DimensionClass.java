package com.outwardInc.uipages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.outwardInc.commonutils.BaseClass;

public class DimensionClass extends BaseClass {

	By cancelButton=By.xpath("//*[@class='owFullscreenCloseButton pointer']");
	By viewTop=By.xpath("//*[@id='owTopDownToggle']");
	By DimensionToggle=By.xpath("//*[@id='owDimensionsToggle']");
	By largerImageCanvasId=By.xpath("//*[@id='owViewport']");
	By hiddenRotationButton=By.xpath("//button[@id ='rotationstatus']");
	By hiddenDimensionToggle=By.xpath("//*[@id='dimensionsstatus']");
	By hiddenTopToggle=By.xpath("//*[@id='topviewstatus']");
	By LargerImageToggle=By.xpath("//*[@id ='owFullscreenToggle']");
	By hiddenZoomToggle=By.xpath("//*[@id='zoomstatus']");
	By zoomFeature=By.xpath("//*[@id='owInlineZoomToggle']");
	
	SoftAssert softAssert = new SoftAssert();
	
	public DimensionClass(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	public void getAttributesOfDimensionToggle() {
		WebElement ele= driver.findElement(hiddenDimensionToggle);
		getDimensionDataFrame(ele);
		Reporter.log("Going to check the Dimension Toggle's Activeness");
		getDimensionToggleHighlightStatus(ele);
	}
	
	public void getDimensionDataFrame(WebElement ele) {
		String dataDimensionFrame=ele.getAttribute("data-dimensionframe");
		System.out.println("The Dimension frames are "+dataDimensionFrame);
		Reporter.log("Going to fetch current Data Dimension Frame Information, The Data Dimension Frame used is : "+dataDimensionFrame);
	}
	
	public void clickDimensionToggle() throws InterruptedException {
		WebElement ele= driver.findElement(DimensionToggle);
		Reporter.log("Going to click on Dimension Toggle");
		clickElement(DimensionToggle);
		Thread.sleep(1900);
	}
	
	public void getDimensionToggleHighlightStatus(WebElement ele) {
		String buttonHighlightStatus=ele.getAttribute("value");
		if(buttonHighlightStatus.equals("on")) {
			Reporter.log("The Dimension button is Highlighted");
			}
		else
			Reporter.log("The Dimension button is not yet Highlighted");
		}
	
//	public void validateDimension() throws InterruptedException {
//		getAttributesOfDimensionToggle();
//		clickDimensionToggle();
//		getAttributesOfDimensionToggle();
//		
//	}
	
	public void validateDimension() throws InterruptedException {
		String currentToggleStatus=null;
		SoftAssert softAssert = new SoftAssert();
		getAttributesOfDimensionToggle();
		WebElement ele= driver.findElement(hiddenDimensionToggle);
		clickDimensionToggle();
		String originalToggleStatus=ele.getAttribute("value");
		softAssert.assertEquals(originalToggleStatus, "on", "The Dimension toggle should be highlighted");
		Reporter.log("The Data Frame used for the Diemsnion is :"+ele.getAttribute("data-dimensionframe"));
		clickDimensionToggle();
		getAttributesOfDimensionToggle();
		currentToggleStatus=ele.getAttribute("value");
		softAssert.assertEquals(currentToggleStatus, "off", "The Dimension toggle should not be highlighted");
		softAssert.assertAll();
	}
	
	
	public void topViewWithdimension() throws InterruptedException {
		SoftAssert softAssert = new SoftAssert();
		boolean ToggleStatus=false;
		clickDimensionToggle();
		WebElement dimensionEle= driver.findElement(hiddenDimensionToggle);
		Reporter.log("Going to click on View Top Toggle");
		clickElement(viewTop);
		WebElement ele= driver.findElement(hiddenTopToggle);
		String dimensionValue=dimensionEle.getAttribute("value");
		String topViewValue=ele.getAttribute("value");
		Reporter.log("The TopView Toggle's Activeness status is: "+topViewValue);
		Reporter.log("The Dimension Toggle's Activeness status is: "+dimensionValue);
		softAssert.assertEquals(dimensionValue, "on", "The Dimension is Missing, As the Toggle is not active");
		softAssert.assertEquals(topViewValue, "on", "The Top View is Missing, As the Toggle is not active");
		if(topViewValue.equals(dimensionValue)) {
			ToggleStatus=true;
			}
		else {
			Reporter.log("Something wrong with the Dimensions");
			ToggleStatus=false;
			}
		softAssert.assertEquals(ToggleStatus, true, " Test Case Failed- The Top View/Dimension is missing, as the status of Toggle is Inactive");
		clickElement(viewTop);
		softAssert.assertAll();
}
}
