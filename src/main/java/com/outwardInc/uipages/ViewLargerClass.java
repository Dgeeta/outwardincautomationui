package com.outwardInc.uipages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.outwardInc.commonutils.BaseClass;

public class ViewLargerClass extends BaseClass{
	
	By cancelButton=By.xpath("//*[@class='owFullscreenCloseButton pointer']");
	By viewTop=By.xpath("//*[@id='owTopDownToggle']");
	By DimensionToggle=By.xpath("//*[@id='owDimensionsToggle']");
	By largerImageCanvasId=By.xpath("//*[@id='owViewport']");
	By hiddenRotationButton=By.xpath("//button[@id ='rotationstatus']");
	By hiddenDimensionToggle=By.xpath("//*[@id='dimensionsstatus']");
	By hiddenTopToggle=By.xpath("//*[@id='topviewstatus']");
	By LargerImageToggle=By.xpath("//*[@id ='owFullscreenToggle']");
	By hiddenZoomToggle=By.xpath("//*[@id='zoomstatus']");
	By zoomFeature=By.xpath("//*[@id='owInlineZoomToggle']");
	
	

		public ViewLargerClass(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}


		
		public void clickHiddenElement(WebElement ele) {
			JavascriptExecutor js = (JavascriptExecutor)driver;
			js.executeScript("arguments[0].click();", ele);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			}
		

		public void verifyLargerImageFuncWithDimension() throws InterruptedException{
			WebElement ele=driver.findElement(hiddenDimensionToggle);
			String value=ele.getAttribute("value");
			Reporter.log("Going to Check if the Dimension Toggle is Switched on or not");	
			}

		public void skuDimensionImage() throws InterruptedException {
			clickElement(DimensionToggle);
			Thread.sleep(2000);

			}
		
					 
		public String[] getCurrentImageAttributes(WebElement ele) {
			Reporter.log("Getting information on Current Image attributes");
			String currentAttributes[] = new String[2];
			String originalImageWidth=ele.getAttribute("width");
			String originalImageHeight=ele.getAttribute("height");
			String currentStyle=ele.getAttribute("style");
			Reporter.log("The Image Dimensions [Height] :"+originalImageWidth);
			Reporter.log("The Image Dimensions [Width] :"+originalImageHeight);
			Reporter.log("Fetching current Image Resolution :"+currentStyle);
			currentAttributes[0]=originalImageWidth;
			currentAttributes[1]=originalImageHeight;
			return (currentAttributes);
			}
			 
		public String[] getNewImageAttributes(WebElement ele) {
			Reporter.log("The Image is in- large View Mode and here are the New Image attributes");
			String newImageWidth=ele.getAttribute("width");
			String newImageHeight=ele.getAttribute("height");
			String newImageattributes[] = new String[2];
			String newStyle=ele.getAttribute("style");
			Reporter.log("The Image Dimensions are :"+newImageWidth);
			Reporter.log("The Image Dimensions are :"+newImageHeight);
			Reporter.log("Fetching New Image Resolution :"+newStyle);
			newImageattributes[0]=newImageWidth;
			newImageattributes[1]=newImageHeight;
			return (newImageattributes);
			}

			 
			 
		public void verifyImageAttributes(String[] currentImage, String[] newImage) {
			SoftAssert softAssert = new SoftAssert();
			Reporter.log("Verifying the Image's attributes when it is Opened in View larger Image Mode");
			Reporter.log("Validating the New Image's attributes with the Original Image's attributes");
			
				 if ((currentImage[0] !=newImage[0]) && (newImage[0] !=null) && 
							(currentImage[1] !=newImage[1]) && (newImage[1] !=null)) {
					 Reporter.log("Here are the Image's attributes when it is opened in -->Larger View Mode- [height] :"+newImage[0]);
					 Reporter.log("Here are the Image's attributes when it is opened in -->Larger View Mode- [Width] :"+newImage[1]);
					 Reporter.log("The New Image's Attributes are not same with the Original Image attributes");
						}
				else {
					Reporter.log("The Image is either Blank or the Image Attributes didn't change");
					} 
				 softAssert.assertNotEquals(currentImage[1], newImage[1], "The Image's Attributes[Width] are similar");
				 softAssert.assertNotEquals(currentImage[0], newImage[0], "The Image's Attributes[height] are similar");
				 softAssert.assertAll();
				 }
		
			 
			 public void validateCancelButtonVisibility() {
				 Reporter.log("Going to validate if the -Cancel Button is available");
				 WebElement elementDisplay= driver.findElement(cancelButton);
				 Boolean value =elementDisplay.isDisplayed();
				 if(value.booleanValue()){
					 Reporter.log("The Cancel button is available in the Page");
						}
					 else {
						 Reporter.log("The Cancel button is not available in the Page");
						  }
				 }
			 
			public void clickCancelButton() {
				Reporter.log("Going to click on -Cancel Button in View Large Image Mode");
				WebElement cancelButtn= driver.findElement(cancelButton);
				cancelButtn.click();
				Reporter.log("Image is back in Normal Mode");
			}
			
			
			public void viewLargerImageFunc() throws InterruptedException{
				WebElement ele= driver.findElement(largerImageCanvasId);
				String[] currentImage=getCurrentImageAttributes (ele);
				Reporter.log("Going to click on - View larger Image");
				clickElement(LargerImageToggle);
				Thread.sleep(1000);
				String[] newImage=getNewImageAttributes(ele);
				verifyImageAttributes(currentImage,newImage);
				validateCancelButtonVisibility();
				clickCancelButton();
				Reporter.log("Test Execution for Click on View large Image Mode Executed Successfully");
				}
			
			public void validateLargerImagewithDimensionToggle() {
				SoftAssert softAssert = new SoftAssert();
				Reporter.log("Going to click on View Larger Image Toggle");
				clickElement(LargerImageToggle);
				WebElement ele= driver.findElement(hiddenDimensionToggle);
				Reporter.log("Going to check the Dimension Toggle's Activeness when Image is viewed in view Larger Mode");
				String value=ele.getAttribute("value");
				Reporter.log("The Dimension Toggle's Activeness status is "+value);
				softAssert.assertEquals(value, "on","The Dimension is missing, as the status of Toggle is Inactive");
				clickCancelButton();
				softAssert.assertAll();
			}
				
			public void validateImagewithTopView() {
				SoftAssert softAssert = new SoftAssert();
				Reporter.log("Going to click on Larger Image Toggle");
					clickElement(LargerImageToggle);
					WebElement ele= driver.findElement(hiddenTopToggle);
					Reporter.log("Going to check the Top View Toggle's Activeness when Image is viewed in view Larger Mode");
					String value=ele.getAttribute("value");
					Reporter.log("The Top view Toggle's Activeness status is "+value);
					softAssert.assertEquals(value, "on","The Top view is missing, as the status of Toggle is Inactive");
					clickCancelButton();	
					softAssert.assertAll();
			}
			
			public void resetToggle() {
				WebElement dimensionToggle= driver.findElement(hiddenDimensionToggle);
				WebElement TopViewToggle= driver.findElement(hiddenTopToggle);
				WebElement ZoomToggle= driver.findElement(hiddenZoomToggle);
				Reporter.log("Going to restore the buttons to OFF state before Starting the Test case");
				if(dimensionToggle.getAttribute("value").equals("on")) {
					clickElement(DimensionToggle);}
				if(TopViewToggle.getAttribute("value").equals("on")) {
					clickElement(viewTop);}
				if(ZoomToggle.getAttribute("value").equals("on")) {
					clickElement(zoomFeature);}
					}
}
