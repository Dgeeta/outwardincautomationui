package com.outwardInc.uipages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.outwardInc.commonutils.BaseClass;


public class SkuPage extends BaseClass{
	
	//By numberOfSkus= By.xpath("//*[@class='owSelectorItem owConfigSelection']");
	By numberOfSkus= By.xpath("//div[contains(@class, 'owSelectorItem owConfigSelection')]");
	By configSku=By.xpath("//span[contains(text(),'Configs : ')] ");
	By configSkuText=By.xpath("//span[contains(text(),'Configs : ')] //following::span");
	By ConfigTitle= By.xpath("//li[@id='debug-config']//li[10]");
	
	
	public SkuPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
		}
	
	
	 public String[][] countNumberOfOmans() throws IOException{
		 String testSheetName=com.outwardInc.commonutils.PropertyFileUtility.getProperty("Test_sheet_name");
		String[][] data= com.outwardInc.commonutils.ExcelUtils.readFromExcel
				(com.outwardInc.commonutils.PropertyFileUtility.getProperty("Oman_path"),testSheetName);
		System.out.println("The Values are "+data[0][0]);
		return data;
		}
	 
	 
	 public int countNumberOfSkus(){
		 List<WebElement> element = driver.findElements(numberOfSkus);
		 int skuCount = element.size();
		 Reporter.log("The No. of Skus are :"+skuCount);
		 return (skuCount);
		 }
	 
	 public String fetchSkuName(int i) {
		 List<WebElement> element = driver.findElements(numberOfSkus);
		 String Sku= element.get(i).getText();
		 element.get(i).click();
		 return(Sku);
		 }
	 
	 public int setMinNumberOfSku(int skuCount, int configSkuCount) {
		 int minSku=0;
		 Reporter.log("Going to find the Min number of Sku to be Executed ");
		 if (configSkuCount>=skuCount) {
			 minSku=skuCount;
			 }
		 else if (skuCount>=configSkuCount) {
			 minSku=configSkuCount;
		 }
		 return minSku;
		  }
	 
	 public void skuContentValidation(String skuName) {
		 SoftAssert softAssert = new SoftAssert();
		 visibilityOfElement(configSku);
		 WebElement ele=driver.findElement(configSkuText);
		 Reporter.log("The Sku's name is "+skuName);
		 Reporter.log("The Content in Config Section is "+ele.getText());
		 Reporter.log("Going the validate the Sku's content");
		 softAssert.assertEquals(ele.getText(), skuName,"The Sku Content doesn't match ");
		 softAssert.assertAll();
		 }
	 }
