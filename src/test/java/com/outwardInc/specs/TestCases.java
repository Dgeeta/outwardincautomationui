package com.outwardInc.specs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.outwardInc.commonutils.ListenerClass;
import com.outwardInc.commonutils.PropertyFileUtility;
import com.outwardInc.uipages.ApplicationLogin;
import com.outwardInc.uipages.DimensionClass;
import com.outwardInc.uipages.RotatorClass;
import com.outwardInc.uipages.SkuPage;
import com.outwardInc.uipages.TopViewClass;
import com.outwardInc.uipages.ViewLargerClass;
import com.outwardInc.uipages.ZoomClass;

@Listeners(ListenerClass.class)
@Test(groups = "WSI-CT3")
public class TestCases {
	private String oman;
	private String composerLink;
	WebDriver driver;
	ApplicationLogin login;
	SkuPage sku;
	ViewLargerClass viewlarger;
	DimensionClass dimension;
	TopViewClass topView;
	ZoomClass zoom;
	RotatorClass rotator;
	String skuName;

	public TestCases(String oman, String composerLink) {
		this.oman = oman;
		this.composerLink = composerLink;
		}
	
	 public WebDriver getDriver() {
	     return driver;
	     }
	
	
	@Test(description="Test Case:Launching Browser and login into the application for the Oman ")
	public void Tc01LaunchBrowser() throws Exception {
		Reporter.log("Going to Launch the Browser for Oman "+oman);
		System.setProperty("webdriver.chrome.driver",".\\src\\main\\resources\\Drivers\\ChromeDriver\\Latest\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(PropertyFileUtility.getProperty("url"));
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		login = new ApplicationLogin(getDriver());
		Reporter.log("Going to login into the Application with "+PropertyFileUtility.getProperty("username")+
				" And Password "+PropertyFileUtility.getProperty("password"));
		login.loginIntoApplication(PropertyFileUtility.getProperty("username"),
				PropertyFileUtility.getProperty("password"));
	}

	@Test(alwaysRun = true,description="Test Case: Going to click on the Composer Link")
	public void Tc02ClickOnComposerLink() throws Exception {
		String ThecomposerUrl = PropertyFileUtility.getProperty("ComposerLink_BaseUrl")+
				PropertyFileUtility.getProperty("product_name")+ oman;
		login = new ApplicationLogin(getDriver());
		Reporter.log("Going to click on the composer link "+ThecomposerUrl);
		login.clickComposerLink(ThecomposerUrl);
        }

	@Test(alwaysRun = true,description="Test Case: Going to get the Total SKUs available")
	public void Tc03FetchSku() throws InterruptedException, IOException {
		Reporter.log("Going to fetch total Sku for "+oman);
		sku=new SkuPage(getDriver());
		int lists = sku.countNumberOfSkus();
		int configuredSkuCount=Integer.parseInt(PropertyFileUtility.getProperty("number_of_sku"));
		int minNumber =sku.setMinNumberOfSku(lists,configuredSkuCount);
		Reporter.log("Total Sku available for "+oman+" is "+lists+
		" And configured SKU count is "+configuredSkuCount);
		Reporter.log("Number of Times the Sku to be Executed: "+minNumber);
        for (int i=0; i<minNumber;i++){
        	Reporter.log(" Going to Execute Test cases for index : "+i+1);
        	skuName=sku.fetchSkuName(i);
        	sku.skuContentValidation(skuName);
        	Tc0401ClickOnViewLargerImage();
        	}
        }

	
	@Test(description="Test Case: To Test the Larger Image functionality")
	public void Tc0401ClickOnViewLargerImage() throws InterruptedException {
		Reporter.log("Going to validate the View Larger Image functionality for Oman "+oman+"And Sku is :"+skuName );
		viewlarger= new ViewLargerClass(getDriver());
		viewlarger.viewLargerImageFunc();
		}
	
	
	@Test(alwaysRun = true,description="Test Case: To Test the Larger Image functionality")
	public void Tc0402ClickOnViewLargerImageWithDimension() throws InterruptedException {
		Reporter.log("Test Case - To Verify that user is able to click on The View Larger Image Toggle with Dimensions on for OMAN: "+oman+"And Sku is: "+skuName );
		dimension= new DimensionClass(getDriver());
		viewlarger.resetToggle();
		dimension.getAttributesOfDimensionToggle();
		dimension.clickDimensionToggle();
		viewlarger.validateLargerImagewithDimensionToggle();
		}

	@Test(alwaysRun = true,description="Test Case - Testing view larger Functionality with TOP VIEW ")
	public void Tc0403ToTestViewLargeImagewithTopView() throws InterruptedException {
		Reporter.log("Test Case - To Verify that user is able to click on The View Larger Image Toggle with Top view for OMAN: "+oman+"And Sku is :"+skuName);
		topView= new TopViewClass(getDriver());
		viewlarger.resetToggle();
		topView.getTopViewAttributeValue();
		topView.getTopViewAttributedataFrame();
		topView.clickOnTopToggle();
		viewlarger.validateImagewithTopView();
		topView.clickOnViewFrontToggle();
		}
		
	
	@Test
	public void Tc0501ToTestZoomInFeature() throws InterruptedException {
	    Reporter.log("Test Case- Testing ZOOM IN feature for OMAN: "+oman+"And Sku is: "+skuName);
	    viewlarger.resetToggle();
	    zoom= new ZoomClass(getDriver());
	    zoom.validationOfZoomToggle();
	    zoom.validationOfZoomOutToggle();
	}
	
	@Test
	public void Tc0502ToTestZoomInFeatureWithDimension() throws InterruptedException {
	     Reporter.log("Test Case- Testing Zoom IN FEATURE WITH DIMENSION for OMAN: "+oman+"And Sku is : "+skuName);
	     viewlarger.resetToggle();
	     zoom.validateZoomImagewithDimensionToggle();
	     }
	
	
	@Test
	public void Tc0503ToTestZoomInFeatureWithTopView() throws InterruptedException  {
	     Reporter.log("TEST CASE-TESTING ZOOM IN FEATURE FOR TOP VIEW IMAGE "+oman+"And Sku is: "+skuName);
	     viewlarger.resetToggle();
	     zoom.verifyZoomImageWithTopView();
	}

	@Test
	public void Tc0504ToTestZoomInFeatureWithTopViewAndDimension() throws InterruptedException  {
	     Reporter.log("TEST CASE-TESTING ZOOM IN FEATURE fOR TOP VIEW IMAGE WITH DIMENSION "+oman+"And Sku is: "+skuName);
	     viewlarger.resetToggle();
	     zoom.validateZoomFeatureWithDimensionAndTopView();
	     }
	
	@Test
	public void Tc0601ToTestDimensionfeature() throws InterruptedException {
	     Reporter.log(" TEST CASE-TESTING DIMENSION FEATURE FOR THE RENDERED IMAGE for OMAN: "+oman+"And Sku is: "+skuName);
	     viewlarger.resetToggle();
		 dimension.validateDimension();
	}
	
	@Test
	public void Tc0602ToTestDimensionfeatureWithLargerView() throws InterruptedException {
		Reporter.log("Test Case - To Verify that user is able to click on The View Larger Image Toggle with Dimensions on for OMAN: "+oman+"And Sku is: "+skuName );
		viewlarger.resetToggle();
		dimension.getAttributesOfDimensionToggle();
		dimension.clickDimensionToggle();
		viewlarger.validateLargerImagewithDimensionToggle();
	}
	
	@Test
	public void Tc0603ToTestDimensionWithZoomIn() throws InterruptedException {
	     Reporter.log("Test Case- Testing  DIMENSION feature with Zoom In Functionality for OMAN: "+oman+"And Sku is: "+skuName);
	     viewlarger.resetToggle();
	     zoom.validateZoomImagewithDimensionToggle();
	     }
	
	@Test
	public void Tc0604ToTestDimensionWithTopView() throws InterruptedException {
	     Reporter.log("Test Case- Testing  DIMENSION feature with Top View Functionality for OMAN: "+oman+"And Sku is: "+skuName);
	     viewlarger.resetToggle();
	     dimension.topViewWithdimension();
	     }
	
	
	@Test
	public void Tc0701ToTestTopView() throws InterruptedException {
	     Reporter.log("Test Case- Testing Top View feature for OMAN: "+oman+"And Sku is: "+skuName);
	     viewlarger.resetToggle();
	     topView.clickOnviewTopImage();
	     }
	
	@Test
	public void Tc0901ToTestImageRotation(){
	    Reporter.log("TEST CASE-TESTING IMAGE ROTATION FOR OMAN: "+oman+"And Sku is: "+skuName);
	    viewlarger.resetToggle();
	    rotator = new RotatorClass(getDriver());
	    Reporter.log("The Rotation Type of the OMAN is : "+rotator.getRotationTypeOfImage());
	    Reporter.log("Here is the List of Framesequence "+ rotator.getFrameSequenceOfImage());
	    rotator.validateImageRotation();
	    }
	
	@Test
	public void Tc0902ToValidateMissingDataFrame(){
		Reporter.log("TEST CASE-To Verify the Missing Data Frames of the Rendered Image for OMAN:"+oman+"And Sku is: "+skuName);
		viewlarger.resetToggle();
		rotator.verifyMissingDataFrame();
	}
	
	@Test
	public void Tc0903ToValidateMissingTopDownFrame(){
		Reporter.log("TEST CASE-To Verify the Missing Top Down Frames of the Rendered Image for OMAN:"+oman +"And Sku is: "+skuName);
		viewlarger.resetToggle();
		rotator.verifyMissingTopDownFrame();
		
	}
	
//	@AfterClass
//	public void Tc12TearUpPage() {
//	driver.close();
//	}
	
}
