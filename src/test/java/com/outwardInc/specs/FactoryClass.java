package com.outwardInc.specs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.testng.IAlterSuiteListener;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import com.outwardInc.commonutils.ExcelUtils;
import com.outwardInc.commonutils.PropertyFileUtility;

public class FactoryClass {

	
	@DataProvider
	public Object[][] countNumberOfOmans() throws IOException{
		 String testSheetName=PropertyFileUtility.getProperty("Test_sheet_name");
		String[][] data= ExcelUtils.readFromExcel(PropertyFileUtility.getProperty("Oman_path"),testSheetName);
		System.out.println("The Values are "+data[0][0]);
		return data;
		}

	@Factory(dataProvider="countNumberOfOmans")
	public Object[] createInstances(String oman, String composerLink) {
		return new Object[] {new TestCases(oman, composerLink)};
		}
}
